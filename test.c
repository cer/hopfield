#include "network.h"
#include <stdio.h>
#include <stdbool.h>
#include <stddef.h>

int main () {
	hNetwork * test = hopfieldInitialize(5);
	int weighttest [2][5] = {{1, -1, 0, 0, 1}, {-1, 0, 0, 1, 0}};
	int testcase [] = {-1, 0, 0, 1, 1};
	if (!hopfieldWeigh(test, (int *) weighttest)) { printf("Issue weighing the network!\n"); }
	int * result = hopfieldUpdate(test, testcase);
	if (result == NULL) {printf("error, could not find similar case!\n");}
	else {printf("success, found %d, %d, %d, %d, %d", result[0], result[1], result[2], result[3], result[4]);}
}
