#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include "network.h"

/* (int) -> hNetwork *
 * This function creates a hNetwork struct and a 2d grid for the weights in memory,
 * and then returns it to the user.  Note that weights gets treated like a * instead
 * of a ** because of how malloc works
 */
hNetwork * hopfieldInitialize(int size){
	hNetwork * net = (struct hopfieldNetwork *) malloc(sizeof(struct hopfieldNetwork));
	net->weights = (int *) malloc(size * size * sizeof(struct hopfieldNetwork));
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size; j++){
			net->weights = 0;
		}
	}
	net->size = size;
	net->wcount = 0;
	net->networkready = false;
	net->timeout = 10000;
	return net;
}

/* (hNetwork *, int *) -> bool
 * This function updates the given hnetwork with the vector.  It does not do the
 * 1/n needed at the end of the combonation function.
 *
 * If this function returns true, the weighing operation was successful.  If it
 * returns false, a mismatch between the vector and network size was detected.*/
bool hopfieldAddWeight(hNetwork * net, int * vect){
	printf("vector %d, %d, %d, %d, %d", vect[0], vect[1], vect[2], vect[3], vect[4]);
	printf("Size: %d, comared to network size %d", sizeof(vect)/sizeof(int), net->size);
	if (sizeof(vect)/sizeof(int) != net->size) { return false; }
	printf("passed size check!\n");
	net->wcount++;
	for(int i = 0; i < net->size; i++){
		for(int j = i + 1; j < net->size; j++){
			printf("updating location %d %d \n", i, j);
			net->weights[i][j] += vect[i] * vect[i];
			net->weights[j][i] = net->weights[i][j];
		}
	}

	return true;
}

/* (hNetwork *) -> bool
 * "Primes" the network for use.  Essentially the network is trained with the
 * operation wij = (1/n)E(qi*qj).  Now you may recall that we do the first
 * part of the operation in the hopfieldaddweight section, but do not ever
 * complete the 1/n step.  This does this, as well as checks the network
 * for issues, such as no added weights, network already readied, and an
 * altered trace.*/
bool hopfieldReadyNetwork(hNetwork * net){
	if (net->wcount < 1){ return false; } /* Dont do it if there aren't any weights added */
	if (net->networkready){ return false; } /* Dont od it if we already primed the network */
	for(int i = 0; i < net->size; i++){ if(net->weights[i][i] != 0) { return false; }} /* Dont do it if we somehow touched the trace */

	for(int i = 0; i < net->size; i++){
		for(int j = i + 1; j < net->size; j++){
			net->weights[i][j] /= net->wcount;
			net->weights[j][i] = net->weights[i][j];
		}
	}

	return true;
}

/* (hNetwork *, int [][], count) -> bool
 * Does both full equation in one operation.  For batch input from a source.
 */
bool hopfieldWeigh(hNetwork * net, int * vect){
	net->patterns = vect;
	for (int i = 0; i < sizeof(vect)/sizeof(vect[0]); i ++){/* can this be done via a null term?  Find out when internet is back. */
		printf("adding item %d to network!\n", i);
		if (!hopfieldAddWeight(net, vect[i])){ return false; }
	}
	printf("added weights to network!\n");
	return hopfieldReadyNetwork(net);
}

/*  */
int * hopfieldUpdate(hNetwork * net, int * vect){
	bool sentry = true;
	int timeout = 0;
	while (sentry){
		srand(time(0));
		int i = rand() % net->size;
		int adder = 0;
		for (int j = 0; j < net->size; j++){
			if (i != j) {
				adder += net->weights[i][j] * vect[j];
			}
		}

		vect[i] = (adder < 0)? -1 : 1;

		/* This might seem spagetti but it works really smoothly, and is very simple/reliable. */
		int n;
		for (int k = 0; k < sizeof(net->patterns)/sizeof(net->patterns[0]); k++){
			for (n = 0; n < net->size; n++){
				if (vect[n] != net->patterns[k][n]) { break; }
			}
			sentry = !(n == net->size);
			if (!sentry) { break ; }
		}
		
		if (timeout > net->timeout){
			printf("COMPARISON FAILURE: REACHED TIMEOUT!\n");
			return NULL;
		}
		timeout++;
	}
	return vect;
}
