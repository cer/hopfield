#ifndef NETWORK_H
#define NETWORK_H

#include <stdbool.h>

typedef struct hopfieldNetwork {
	int ** weights;
	int size;
	int ** patterns;
	int wcount;
	bool networkready;
	int timeout;
} hNetwork;

hNetwork * hopfieldInitialize(int size); /* build the network itself, initialize in memory, ect */
bool hopfieldAddWeight(hNetwork * net, int * vect); /* Used for first operation in weighing network. */
bool hopfieldReadyNetwork(hNetwork * net); /* does 1/wcount operation to all values of the network, and checks for issues */
bool hopfieldWeigh(hNetwork * net, int * vect); /* Used to train the network with data. */
int * hopfieldUpdate(hNetwork * net, int * vect); /* Update the network with a new vector until convergence */

#endif
