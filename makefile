network:
	gcc -c network.h
	gcc -c network.c

test:
	gcc -c test.c

all: network test
	gcc network.o test.o -o networktest.out

run: all
	./networktest.out

clean:
	rm *.o
	rm *.gch
	rm *.out
